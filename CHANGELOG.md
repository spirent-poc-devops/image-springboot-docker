# <img src="https://upload.wikimedia.org/wikipedia/en/thumb/c/ce/Spirent_logo.svg/490px-Spirent_logo.svg.png" alt="Spirent Logo" width="200"> <br/> SpringBoot Build and Test Base Image ChangeLog

### 1.0.0 (2021-08-26)

Initial release for Maven 3 and OpenJDK 16

#### Features
* OpenJDK 16
* Maven 3

#### Breaking Changes
No breaking changes since this is the first version

#### Bug Fixes
No fixes in this version
